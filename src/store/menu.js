export default {
  state: {
    mainMenu: [
      { route: '/', title: 'V Music' },
      { route: '/albums', title: 'Albums' },
      { route: '/tracks', title: 'Tracks' }
    ]
  },
  getters: {
    menu (state) {
      return state.mainMenu
    }
  }
}
