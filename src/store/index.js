import Vue from 'vue'
import Vuex from 'vuex'

import menu from './menu'
import musicPlayer from './musicPlayer'

import feeds from './api/feeds'
import albums from './api/albums'
import tracks from './api/tracks'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    menu,
    musicPlayer,
    feeds,
    albums,
    tracks
  }
})
