import axios from 'axios'

export default {
  state: {
    feeds: {}
  },
  mutations: {
    storeFeeds (state, feeds) {
      state.feeds = feeds
    }
  },
  getters: {
    feeds (state) {
      return state.feeds
    }
  },
  actions: {
    getFeeds ({ commit }) {
      let url = `${process.env.VUE_APP_API_URL}/feeds`

      axios
        .get(url)
        .then(response => {
          commit('storeFeeds', response.data)
        })
        .catch(error => {
          console.error(error)
        })
    }
  }
}
