export default {
  state: {
    playlist: [],
    currentIdx: 0
  },
  mutations: {
    addMusicToPlaylist (state, music) {
      state.playlist.push(music)

      localStorage.setItem('playlist', JSON.stringify(state.playlist))
    },
    removeMusicFromPlaylistByIdx (state, idx) {
      state.playlist.splice(idx, 1)

      localStorage.setItem('playlist', JSON.stringify(state.playlist))
    },
    changePlaylistIndex (state, idx) {
      state.currentIdx = idx
    },
    replacePlaylist (state, newPlaylist) {
      state.playlist = newPlaylist
    }
  },
  actions: {
    addMusicToPlaylist ({ commit }, music) {
      commit('addMusicToPlaylist', music)
    },
    nextMusic ({ commit, state }) {
      let newId = state.currentIdx + 1

      if (newId > state.playlist.length - 1) {
        newId = 0
      }

      commit('changePlaylistIndex', newId)
    },
    prevMusic ({ commit, state }) {
      let newId = state.currentIdx - 1

      if (newId < 0) {
        newId = state.playlist.length - 1
      }

      commit('changePlaylistIndex', newId)
    },
    changeMusicByIdx ({ commit }, id) {
      commit('changePlaylistIndex', id)
    },
    getPlaylistFromStore ({ commit }) {
      let playlist = localStorage.getItem('playlist')

      playlist = JSON.parse(playlist)

      if (playlist) {
        commit('replacePlaylist', playlist)
      }
    },
    removePlaylistItemByIdx ({ commit, state }, idx) {
      let shouldAdjustIndex = state.currentIdx > idx

      if (shouldAdjustIndex) {
        commit('changePlaylistIndex', state.currentIdx - 1)
      }

      commit('removeMusicFromPlaylistByIdx', idx)
    }
  },
  getters: {
    currentMusic (state) {
      return state.playlist[state.currentIdx]
    },
    playlist (state) {
      return state.playlist
    },
    currentMusicIdx (state) {
      return state.currentIdx
    }
  }
}
