import pad from 'pad-left'

export default {
  /**
   * Formats amount of seconds into format MM:SS
   * @param {Number} _seconds Seconds to format to
   *
   * @returns {String} MM:SS formatted string
   */
  formatTimeFromSeconds (_seconds) {
    let minutes = Math.floor(_seconds / 60)
    let seconds = Math.floor(_seconds - minutes * 60)

    return `${pad(minutes, 2, 0)}:${pad(seconds, 2, 0)}`
  }
}
