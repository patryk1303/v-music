import Vue from 'vue'
import axios from 'axios'

export default {
  state: {
    albums: {},
    albumsData: {}
  },
  mutations: {
    storeAlbums (state, albums) {
      state.albums = albums
    },
    storeAlbum (state, albumData) {
      if (albumData.results && albumData.results.length) {
        let album = albumData.results[0]
        Vue.set(state.albumsData, album.id, album)
      }
    }
  },
  getters: {
    albums (state) {
      return state.albums
    },
    getAlbumById (state) {
      return function (id) {
        return state.albumsData[id]
      }
    }
  },
  actions: {
    getAlbums ({ commit }, namesearch = '') {
      const url = `${process.env.VUE_APP_API_URL}/albums`
      let params = {
        limit: '12'
      }

      if (namesearch.length) {
        params.namesearch = namesearch
      }

      axios.get(url, { params })
        .then(response => {
          commit('storeAlbums', response.data)
        })
        .catch(error => {
          console.error(error)
        })
    },
    getAlbumById ({ commit, dispatch }, albumId = -1) {
      const url = `${process.env.VUE_APP_API_URL}/albums`
      let params = {
        id: [
          albumId
        ]
      }

      axios.get(url, { params })
        .then(response => {
          commit('storeAlbum', response.data)

          dispatch('getTracksByAlbumId', response.data.results[0].id)
        })
        .catch(error => {
          console.error(error)
        })
    }
  }
}
