import axios from 'axios'
import Vue from 'vue'
import { library } from '@fortawesome/fontawesome-svg-core'
import * as faIcons from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

import App from './App.vue'
import router from './router'
import store from './store/'
import './registerServiceWorker'

import '../node_modules/bulma/bulma.sass'

console.log(faIcons)
let usedIcons = [
  faIcons.faClock,
  faIcons.faSearch,
  faIcons.faPlus,
  faIcons.faPlay,
  faIcons.faPause,
  faIcons.faChevronRight,
  faIcons.faChevronLeft,
  faIcons.faChevronUp,
  faIcons.faChevronDown,
  faIcons.faMusic,
  faIcons.faTimes,
  faIcons.faDownload
]
library.add(...usedIcons)

Vue.component('fa', FontAwesomeIcon)

Vue.config.productionTip = false

axios.interceptors.request.use(config => {
  if (config.params) {
    config.params.client_id = process.env.VUE_APP_CLIENT_ID
  } else {
    config.params = {
      client_id: process.env.VUE_APP_CLIENT_ID
    }
  }

  return config
}, error => {
  return Promise.reject(error)
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
