import Vue from 'vue'
import axios from 'axios'

export default {
  state: {
    tracksData: {},
    tracksSearch: {}
  },
  mutations: {
    storeTrackData (state, tracksData) {
      for (let trackData of tracksData) {
        let trackId = trackData.id
        Vue.set(state.tracksData, trackId, trackData)
      }
    },
    storeTrackSearch (state, tracksData) {
      state.tracksSearch = {}

      for (let trackData of tracksData) {
        let trackId = trackData.id
        Vue.set(state.tracksSearch, trackId, trackData)
      }
    }
  },
  actions: {
    fetchTrackById ({ commit }, id) {
      const url = `${process.env.VUE_APP_API_URL}/tracks`
      let params = {
        limit: '12',
        id: [
          id
        ]
      }

      axios.get(url, { params })
        .then(response => {
          commit('storeTrackData', response.data.results)
          commit('storeTrackSearch', response.data.results)
        })
        .catch(error => {
          console.error(error)
        })
    },
    getTracksByAlbumId ({ commit }, albumId) {
      const url = `${process.env.VUE_APP_API_URL}/tracks`
      let params = {
        album_id: [
          albumId
        ]
      }

      axios.get(url, { params })
        .then(response => {
          if (response.data) {
            commit('storeTrackData', response.data.results)
          }
        })
        .catch(error => {
          console.error(error)
        })
    },
    getTracks ({ commit }, namesearch = '') {
      const url = `${process.env.VUE_APP_API_URL}/tracks`
      let params = {
        limit: '12'
      }

      if (namesearch.length) {
        params.namesearch = namesearch
      }

      axios.get(url, { params })
        .then(response => {
          commit('storeTrackData', response.data.results)
          commit('storeTrackSearch', response.data.results)
        })
        .catch(error => {
          console.error(error)
        })
    }
  },
  getters: {
    getTrackById (state) {
      return function (id) {
        let hasData = state.tracksData && state.tracksData[id]

        return hasData
          ? state.tracksData[id]
          : false
      }
    },
    getTracksByAlbumId (state) {
      return function (albumId) {
        let trackByAlbum = {}
        for (let trackKey in state.tracksData) {
          let trackData = state.tracksData[trackKey]
          if (trackData.album_id === albumId) {
            trackByAlbum[trackData.id] = trackData
          }
        }
        return trackByAlbum
      }
    },
    getSearchTracks (state) {
      return state.tracksSearch
    }
  }
}
