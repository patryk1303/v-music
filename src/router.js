import Vue from 'vue'
import Router from 'vue-router'

import Home from './views/Home.vue'
import Albums from './views/Albums.vue'
import AlbumDetails from './views/AlbumDetails.vue'
import Tracks from './views/Tracks.vue'
import TrackDetails from './views/TrackDetails.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/albums',
      name: 'albums',
      component: Albums
    },
    {
      path: '/albums/:albumId',
      name: 'albumDetails',
      component: AlbumDetails
    },
    {
      path: '/tracks',
      name: 'tracks',
      component: Tracks
    },
    {
      path: '/tracks/:trackId',
      name: 'trackDetails',
      component: TrackDetails
    }
  ],
  linkActiveClass: 'is-active'
})
